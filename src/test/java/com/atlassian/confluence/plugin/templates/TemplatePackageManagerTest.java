package com.atlassian.confluence.plugin.templates;

import com.atlassian.confluence.pages.templates.PageTemplate;
import com.atlassian.confluence.plugin.templates.export.TemplatePackage;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * @author Ryan Thomas
 */
public class TemplatePackageManagerTest {
    private static final String TEST_NAME = "Test Name";

    @Mock private TemplatePackage templatePackage;
    @Mock private PageTemplate pageTemplate;

    private TemplatePackageManager manager;

    @Before
    public void beforeEachTest() throws Exception {
        initMocks(this);

        List<TemplatePackage> packages = new ArrayList<TemplatePackage>();
        packages.add(templatePackage);

        List<PageTemplate> templates = new ArrayList<PageTemplate>();
        templates.add(pageTemplate);

        when(templatePackage.getAvailableTemplates()).thenReturn(templates);
        when(templatePackage.getPackageName()).thenReturn(TEST_NAME);

        manager = new TemplatePackageManager(packages);
    }

    @Test
    public void testGetAvailableTemplates() throws Exception {
        Map<String, List<PageTemplate>> packages = manager.getAvailableTemplates();
        verify(templatePackage).getAvailableTemplates();
        assertTrue(packages.containsKey(TEST_NAME));
        assertEquals(1, packages.size());
    }

    @Test
    public void testGetTemplatesByHash() {
        Map<String, PageTemplate> hashes = manager.getTemplatesByHash();
        assertEquals(1, hashes.size());
    }
}
