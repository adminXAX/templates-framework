package com.atlassian.confluence.plugin.templates.config;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.plugin.templates.TemplatePackageManager;
import com.atlassian.confluence.setup.settings.SettingsManager;

import java.util.ArrayList;

/**
 * @author Ryan Thomas
 */
public class AjaxTemplateRendererAction extends ConfluenceActionSupport {
    private TemplatePackageManager templatePackageManager;
    private String hashString;
    private String template;
    private SettingsManager settingsManager;

    public String render() throws Exception {
        if(templatePackageManager.getTemplatesByHash().containsKey(hashString)) {
            String wikiMarkup = templatePackageManager.getTemplatesByHash().get(hashString).getContent();
            template = templatePackageManager.getXhtmlContent().convertWikiToView(
                    wikiMarkup,
                    new DefaultConversionContext(settingsManager.getGlobalDescription().toPageContext()),
                    new ArrayList<RuntimeException>()
            );
        }
        else {
            template = getText("ajax.error", new String[] {hashString});
        }
        return SUCCESS;
    }

    public String markup() {
        if(templatePackageManager.getTemplatesByHash().containsKey(hashString)) {
            template = templatePackageManager.getTemplatesByHash().get(hashString).getContent().replaceAll("\\n", "<br>");
        }
        else {
            template = getText("ajax.error", new String[] {hashString});
        }
        return SUCCESS;
    }

    public void setTemplatePackage(TemplatePackageManager templatePackageManager) {
        this.templatePackageManager = templatePackageManager;
    }

    public void setHashString(String hashString) {
        this.hashString = hashString;
    }

    public void setSettingsManager(SettingsManager settingsManager) {
        this.settingsManager = settingsManager;
    }

    public String getTemplate() {
        return template;
    }
}
