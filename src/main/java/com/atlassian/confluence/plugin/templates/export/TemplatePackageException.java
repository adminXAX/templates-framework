package com.atlassian.confluence.plugin.templates.export;

public class TemplatePackageException extends Exception {
    public TemplatePackageException() {
        super();
    }

    public TemplatePackageException(String message) {
        super(message);
    }

    public TemplatePackageException(String message, Throwable cause) {
        super(message, cause);
    }
}
