package com.atlassian.confluence.plugin.templates.config;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.pages.templates.PageTemplate;
import com.atlassian.confluence.plugin.templates.TemplatePackageManager;
import com.atlassian.confluence.spaces.Space;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class ConfigureTemplatesAction extends ConfluenceActionSupport {
    private TemplatePackageManager templatePackageManager;
    private String spaceKey;
    private String[] toInstall;
    private List<String> installed;
    private String status;

    public String input() {
        return INPUT;
    }

    public String install() {
        if(ArrayUtils.isEmpty(toInstall)) {
            status = "noinput";
            return INPUT;
        }
        
        Space destination = null;
        if(!StringUtils.isEmpty(spaceKey)) {
            destination = templatePackageManager.getSpaceManager().getSpace(spaceKey);
        }

        installed = new ArrayList<String>();
        Map<String, PageTemplate> byHash = templatePackageManager.getTemplatesByHash();
        for(String templateHashCode : toInstall) {
            if(byHash.containsKey(templateHashCode)) {
                PageTemplate template = byHash.get(templateHashCode);
                template.setSpace(destination);
                templatePackageManager.getTemplateManager().savePageTemplate(template, null);
                installed.add(template.getName());                
            }
        }

        status = "success";
        return SUCCESS;
    }

    public String getStatus() {
        return status;
    }

    public List<String> getInstalled() {
        return installed;
    }

    public String getActionName(String className) {
        return "Configure Template Packages Plugin";
    }

    public String getAdminUrl() {
        StringBuilder adminUrl = new StringBuilder("pages/templates2/listpagetemplates.action");
        if(!StringUtils.isEmpty(spaceKey)) {
            adminUrl.append("?key=").append(spaceKey);
        }
        return adminUrl.toString();
    }

    public String getSpaceName() {
        if(StringUtils.isEmpty(spaceKey)) {
            return getText("template.config.global.space");
        }
        else {
            return templatePackageManager.getSpaceManager().getSpace(spaceKey).getName();
        }
    }

    public void setSpaceKey(String spaceKey) {
        this.spaceKey = spaceKey;
    }

    public void setToInstall(String[] toInstall) {
        this.toInstall = toInstall;
    }

    public void setTemplatePackage(TemplatePackageManager templatePackageManager) {
        this.templatePackageManager = templatePackageManager;
    }

    public List<Space> getSpaces() {
        return templatePackageManager.getSpaceManager().getAllSpaces();
    }

    public List<PageTemplate> getInstalledTemplates() {
        return templatePackageManager.getInstalledTemplates();
    }

    public Set<String> getInstalledTemplatesNames() {
        Set<String> names = new TreeSet<String>();
        for(PageTemplate t : getInstalledTemplates()) {
            names.add(t.getName());
        }
        return names;
    }

    public Set<String> getConflictNames() {
        Set<String> names = new TreeSet<String>();
        Set<String> conflictSet = new TreeSet<String>();
        for(Map.Entry<String, List<PageTemplate>> entry : templatePackageManager.getAvailableTemplates().entrySet()) {
            for(PageTemplate t : entry.getValue()) {
                if(names.contains(t.getName())) {
                    conflictSet.add(t.getName());
                }
                names.add(t.getName());
            }
        }
        return conflictSet;
    }

    public Map<String, List<PageTemplate>> getAvailableTemplates() {
        Map<String, List<PageTemplate>> templates = templatePackageManager.getAvailableTemplates();
        if(null == templates)
        {
            templates = Collections.emptyMap();
        }
        return templates;
    }
}
