package com.atlassian.confluence.plugin.templates;

import com.atlassian.confluence.pages.templates.PageTemplate;
import com.atlassian.confluence.pages.templates.PageTemplateManager;
import com.atlassian.confluence.plugin.templates.export.TemplatePackage;
import com.atlassian.confluence.plugin.templates.export.TemplatePackageException;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TemplatePackageManager {
    private static final Logger log = LoggerFactory.getLogger(TemplatePackageManager.class);

    private PageTemplateManager templateManager;
    private Iterable<TemplatePackage> packages;
    private SpaceManager spaceManager;
    private XhtmlContent xhtmlContent;

    public TemplatePackageManager(Iterable<TemplatePackage> packages) {
        this.packages = packages;
        if(log.isDebugEnabled()) {
            for(TemplatePackage p : packages) {
                log.debug("Template Package: " + p.getPackageName());
            }
        }
    }

    public void setSpaceManager(SpaceManager spaceManager) {
        this.spaceManager = spaceManager;
    }

    public void setTemplateManager(PageTemplateManager templateManager) {
        this.templateManager = templateManager;
    }

    public PageTemplateManager getTemplateManager() {
        return templateManager;
    }

    @SuppressWarnings("unchecked")
    public List<PageTemplate> getInstalledTemplates() {
        return templateManager.getGlobalPageTemplates();
    }

    public SpaceManager getSpaceManager() {
        return spaceManager;
    }

    public XhtmlContent getXhtmlContent() {
        return xhtmlContent;
    }

    public void setXhtmlContent(XhtmlContent xhtmlContent) {
        this.xhtmlContent = xhtmlContent;
    }

    public Map<String, List<PageTemplate>> getAvailableTemplates() {
        Map<String, List<PageTemplate>> templates = new HashMap<String, List<PageTemplate>>();
        for (TemplatePackage tPackage : packages)
        {
            try
            {
                List<PageTemplate> pluginTemplates = tPackage.getAvailableTemplates();
                if (pluginTemplates != null)
                {
                    templates.put(tPackage.getPackageName(), pluginTemplates);
                }
                else
                {
                    log.error("null Template package from: {}", tPackage);
                }
            }
            catch (TemplatePackageException e)
            {
                log.error("Unable to get templates from: " + tPackage.getPackageName(), e);
            } catch (RuntimeException re)
            {
                log.error("Unable to get templates from: " + tPackage.getPackageName(), re);
            }
        }

        return templates;
    }

    public Map<String, PageTemplate> getTemplatesByHash() {
        Map<String, PageTemplate> byHash = new HashMap<String, PageTemplate>();
        for(Map.Entry<String, List<PageTemplate>> pkgEntry : getAvailableTemplates().entrySet()) {
            for(PageTemplate template : pkgEntry.getValue()) {
                byHash.put(String.valueOf(template.hashCode()), template);
            }
        }
        return byHash;
    }
}
