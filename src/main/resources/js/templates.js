AJS.$(document).ready(function() {
});

function htmlError(e) {
    return "<p class='error'>" + e + "</p>";
}

function showTemplateDialog(hashCode, name) {
    var popup = new AJS.Dialog(860, 530, "view-template-dialog");
    popup.addHeader(name);
    popup.addPanel("Template Preview", "panel1");

    var ajaxUrl = "renderTemplate.action?hashString=" + hashCode;
    AJS.$.ajax({
        url: ajaxUrl,
        success: function(data) {
            popup.gotoPanel(0);
            popup.getCurrentPanel().html(data);
            popup.gotoPanel(0);
        },
        error: function(e) {
            popup.gotoPanel(0);
            popup.getCurrentPanel().html(htmlError(e));
            popup.gotoPanel(0);
        }
    });

    popup.addPanel("Wiki Markup", "panel2");
    ajaxUrl = "getMarkup.action?hashString=" + hashCode;
    AJS.$.ajax({
        url: ajaxUrl,
        success: function(data) {
            popup.gotoPanel(1);
            popup.getCurrentPanel().html(data);
            popup.gotoPanel(0);
        },
        error: function(e) {
            popup.gotoPanel(1);
            popup.getCurrentPanel().html(htmlError(e));
            popup.gotoPanel(0);
        }
    });

    popup.addButton("Close", function (dialog) {
        dialog.hide();
    });
    popup.popup.element.find(".button-panel").append(AJS.renderTemplate("template-help-link"));
    popup.gotoPage(0);
    popup.gotoPanel(0);
    popup.show();
}

function uncheckAll(pkg) {
    eval("var boxes = document.configuration.toInstall" + pkg);
    for (i = 0; i < boxes.length; i++) {
        boxes[i].checked = false ;
    }
}
function checkAll(pkg) {
    eval("var boxes = document.configuration.toInstall" + pkg);
    for (i = 0; i < boxes.length; i++) {
        boxes[i].checked = true ;
    }
}